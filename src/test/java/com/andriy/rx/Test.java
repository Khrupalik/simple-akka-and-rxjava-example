package com.andriy.rx;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.andriy.akka.Master;
import com.andriy.akka.WriterActor;
import com.andriy.akka.messages.Start;
import rx.Observable;
import rx.Subscriber;
import rx.observers.TestSubscriber;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 09.11.15.
 */
public class Test {
    public static String FILE_NAME = "data.txt";

    @org.junit.Test
    public void rxJavaTest() {

        Observable<Object> objectObservable = getObservable();
        TestSubscriber<Object> testSubscriber = getTestSubscriber();
//        Subscriber<Object> mySubscriber = getSubscriber()

        objectObservable.subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();

    }

    @org.junit.Test
    public void akkaTest(){
        ActorSystem actorSystem = ActorSystem.create("system");

        ActorRef master = actorSystem.actorOf(new Props(Master.class), "master");
        actorSystem.actorOf(new Props(WriterActor.class), "writer");
        master.tell(new Start(FILE_NAME));
        actorSystem.awaitTermination();
    }



    public Observable<Object> getObservable() {
        return Observable.create((subscriber) -> {
            System.out.println("Parse start..");

            try (BufferedReader file = new BufferedReader(new FileReader(new File(FILE_NAME)))) {

                Map<Integer, Double> result = new HashMap<>();
                String line;
                while ((line = file.readLine()) != null) {

                    String arr[] = line.split(":");
                    int key = Integer.valueOf(arr[0]);
                    double value = Double.valueOf(arr[1]);
                    result.put(key, result.getOrDefault(key, 0.0) + value);
                }

                FileWriter fileWriter = new FileWriter("result_rx.txt");
                result.forEach((k, v) -> {
                    try {
                        fileWriter.write(k + ":" + v + "\n");
                    } catch (IOException e) {
                        subscriber.onError(e);
                    }
                });

                fileWriter.close();
                subscriber.onCompleted();

            } catch (IOException e) {
                subscriber.onError(e);
                e.printStackTrace();
            }

        });

    }

    public Subscriber<Object> getSubscriber() {
        return new Subscriber<Object>() {
            @Override
            public void onNext(Object s) {
                System.out.println(s);
            }

            @Override
            public void onCompleted() {
                System.out.println("Parse complete!");
                unsubscribe();
            }

            @Override
            public void onError(Throwable e) {
                System.err.println("Some error.. {" + e.toString() + "}");
            }
        };
    }

    public TestSubscriber<Object> getTestSubscriber() {
        return new TestSubscriber<>(getSubscriber()) ;
    }

}
