package com.andriy.akka.messages;

import java.util.Map;

/**
 * Created by root on 09.11.15.
 */
public class WriteToFile {
    public final String fileName;
    public final Map<Integer, Double> data;

    public WriteToFile(String fileName, Map<Integer, Double> data) {
        this.fileName = fileName;
        this.data = data;
    }
}
