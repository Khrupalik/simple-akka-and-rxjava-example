package com.andriy.akka.messages;

import java.util.Optional;

/**
 * Created by root on 09.11.15.
 */
public class Status {
    public final int code;
    public Optional<String> error;

    public Status(int status) {
        this.code = status;
    }

    public Status(int status, String error) {
        this.code = status;
        this.error = Optional.of(error);
    }
}