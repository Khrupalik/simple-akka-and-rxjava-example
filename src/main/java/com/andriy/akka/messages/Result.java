package com.andriy.akka.messages;

import java.util.Optional;

/**
 * Created by root on 09.11.15.
 */
public class Result {
    public final Status status;
    public Optional<String> fileName;

    public Result(Status status) {
        this.status = status;
    }

    public Result(Status status, String fileName) {
        this.status = status;
        this.fileName = Optional.of(fileName);
    }
}


