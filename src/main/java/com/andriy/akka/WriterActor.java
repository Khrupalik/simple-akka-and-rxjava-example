package com.andriy.akka;

import akka.actor.UntypedActor;
import com.andriy.akka.messages.*;

import java.io.*;

/**
 * Created by root on 09.11.15.
 */
public class WriterActor extends UntypedActor {
    @Override
    public void onReceive(Object message) throws Exception {

        if(message instanceof com.andriy.akka.messages.WriteToFile) {

            com.andriy.akka.messages.WriteToFile data = (com.andriy.akka.messages.WriteToFile)message;
            FileWriter fileWriter = new FileWriter(new File(data.fileName));
            data.data.forEach((k, v) -> {
                try {
                    fileWriter.write(k + ":" + v + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            fileWriter.close();
            getSender().tell(new Result(new Status(0), data.fileName), getSelf());

        }


    }
}
