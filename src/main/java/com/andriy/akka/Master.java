package com.andriy.akka;

import akka.actor.UntypedActor;

import java.io.*;
import java.util.*;
import com.andriy.akka.messages.*;

/**
 * Created by root on 08.11.15.
 */
public class Master extends UntypedActor {


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Start) {
            Start start = (Start) message;
            BufferedReader file = new BufferedReader(new FileReader(new File(start.fileName)));
            System.out.println("Parse start..");
            Map<Integer, Double> result = new HashMap<>();
            String line;
            while ((line = file.readLine()) != null) {

                String arr[] = line.split(":");
                int key = Integer.valueOf(arr[0]);
                double value = Double.valueOf(arr[1]);
                result.put(key, result.getOrDefault(key, 0.0) + value);
            }

            context().system().actorSelection("/user/writer").tell(new com.andriy.akka.messages.WriteToFile("result_akka.txt", result), getSelf());
        } else if(message instanceof Result) {
            Result result = (Result) message;
            if(result.status.code != 0) {
                System.err.println("Ops.. Some error: " + result.status.error.get());
            } else {
                System.out.println("Parse complete! Result in '" + result.fileName.get() + "'");
            }

            context().system().shutdown();
        }

    }
}


